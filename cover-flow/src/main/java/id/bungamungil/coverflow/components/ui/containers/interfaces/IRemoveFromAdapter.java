package id.bungamungil.coverflow.components.ui.containers.interfaces;


public interface IRemoveFromAdapter{
	void removeItemFromAdapter(int position);
}
